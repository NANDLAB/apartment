import java.util.List;

public class Bathroom extends Room {
    private boolean shower;

    public Bathroom(List<Room> neighbours) {
        super("Bathroom", neighbours);
        shower = false;
    }
    public boolean getShower() {
        return shower;
    }
    public void setShower(boolean shower) {
        this.shower = shower;
    }
}
