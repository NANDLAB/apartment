import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scannerIn = new Scanner(System.in);
        ArrayList<Room> nb = new ArrayList<>();
        Room hallway = new Room("Hallway", null);
        nb.add(hallway);
        Room workroom = new Room("Workroom", nb);
        nb.add(workroom);
        Room bedroom = new Room("Bedroom", nb);
        nb.remove(1);
        Room kitchen = new Kitchen(nb);
        Room bathroom = new Bathroom(nb);
        Apartment apartment = new Apartment(hallway);
        apartment.extension(workroom);
        apartment.extension(bedroom);
        apartment.extension(kitchen);
        apartment.extension(bathroom);
        apartment.enter(scannerIn);
        scannerIn.close();
    }
}
